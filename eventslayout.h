#ifndef EVENTSLAYOUT_H
#define EVENTSLAYOUT_H

#include "Arduino.h"
#include "serialpackage.h"

class EventsLayout
{
public:
    explicit EventsLayout(char *buffer_write, char *buffer_read, uint16_t buffer_size);
    void dispatch(const char *message);
    bool poll();
    const char *respond();
    
private:
    bool m_flagReady;
    bool m_flagMotor;
    char *m_buffer_write;
    char *p_buffer_write;
    char *m_buffer_read;
    const char *p_buffer_read;
    uint8_t m_handwheelState;
    uint16_t m_buffer_size;
    uint16_t m_feed;
    uint16_t m_speedCut;
    uint16_t m_speedReturn;
    uint32_t m_feedAdvance;
    uint32_t m_timestampHandwheel;

    void action_error(uint8_t errorCode);
    void action_systemGetPartId(void);
    void action_systemGetVersion(void);
    void action_stepperFeedrateMotorControl(void);
    void action_stepperFeed(void);
    void action_cutterMotorControl(void);
    void action_cutterSpeedCutting(void);
    void action_cutterSpeedReturn(void);
    void action_cutterHandwheelPosition(void);

    const uint8_t SYSTEM_ADDRESS = 0x81;
    const uint8_t SYSTEM_RESPOND = 0x18;
    const uint8_t SYSTEM_CODE_GetPartId = 0xF1;
    const uint8_t SYSTEM_CODE_CommandTransmissionError = 0xF3;
    const uint8_t SYSTEM_CODE_GetVersion = 0xF5;

    const uint8_t STEPPER_ADDRESS = 0x41;
    const uint8_t STEPPER_RESPOND = 0x14;
    const uint8_t STEPPER_CODE_FeedrateMotorControl = 0x20;
    const uint8_t STEPPER_CODE_Feed = 0x23;
    const uint32_t STEPPER_FeedrateMaxPosition = 200000;

    const uint8_t CUTTER_ADDRESS = 0x51;
    const uint8_t CUTTER_RESPOND = 0x15;
    const uint8_t CUTTER_CODE_CuttingMotorControl = 0x20;
    const uint8_t CUTTER_CODE_CuttingSpeed = 0x30;
    const uint8_t CUTTER_CODE_ReturnSpeed = 0x31;
    const uint8_t CUTTER_CODE_HandwheelPosition = 0x40;

    const uint8_t STATUS_NULL = 0x00;
    const uint8_t STATUS_SET = 0x01;
    const uint8_t STATUS_GET = 0xFF;
    const uint8_t STATUS_InvalidCalibration = 0xE0;

    static const uint8_t action_map_size = 9;
    typedef void (EventsLayout::*ActionCallback)(void);
    struct ActionMap {
        uint8_t address;
        uint8_t code;
        ActionCallback callback;
    };

    ActionMap m_actions[action_map_size] = {
        {SYSTEM_ADDRESS, SYSTEM_CODE_GetPartId, &EventsLayout::action_systemGetPartId},
        {SYSTEM_ADDRESS, SYSTEM_CODE_GetVersion, &EventsLayout::action_systemGetVersion},
        {STEPPER_ADDRESS, STEPPER_CODE_FeedrateMotorControl, &EventsLayout::action_stepperFeedrateMotorControl},
        {STEPPER_ADDRESS, STEPPER_CODE_Feed, &EventsLayout::action_stepperFeed},
        {CUTTER_ADDRESS, CUTTER_CODE_CuttingMotorControl, &EventsLayout::action_cutterMotorControl},
        {CUTTER_ADDRESS, CUTTER_CODE_CuttingSpeed, &EventsLayout::action_cutterSpeedCutting},
        {CUTTER_ADDRESS, CUTTER_CODE_ReturnSpeed, &EventsLayout::action_cutterSpeedReturn},
        {CUTTER_ADDRESS, CUTTER_CODE_HandwheelPosition, &EventsLayout::action_cutterHandwheelPosition},
        {0, 0, 0}
    };


};

#endif /* EVENTSLAYOUT_H */
