#ifndef SERIALPORT_H
#define SERIALPORT_H

#include "Arduino.h"

class SerialPort
{
public:
    explicit SerialPort(char *buffer, const uint16_t bufferSize);

    void open(const uint32_t baudRate);
    bool poll();
    void send(const char *message);
    const char *message();

private:
    bool m_flagReceiving;
    uint16_t m_bufferIndex;
    char *m_buffer;
    uint16_t m_bufferSize;
    
};

#endif /* SERIALPORT_H */
