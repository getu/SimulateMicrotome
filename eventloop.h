#ifndef EVENTLOOP_H
#define EVENTLOOP_H

#include "Arduino.h"
#include "serialpackage.h"

class EventLoop
{
public:
    explicit EventLoop(char *buffer, const uint16_t bufferSize);
    void dispatch(const char *message);
    bool process();
    const char *respond();

private:
    bool m_flagReady;
    bool m_flagMotor;
    char *m_buffer;
    const char *p_buffer_read;
    char *p_buffer_write;
    uint8_t m_handwheelState;
    uint16_t m_bufferSize;
    uint16_t m_feed;
    uint16_t m_speedCut;
    uint16_t m_speedReturn;
    uint32_t m_feedPosition;
    uint32_t m_timestampHandwheel;

    void action_error(uint8_t errorCode);
    void action_systemGetPartId(void);
    void action_systemGetVersion(void);
    void action_stepperFeedrateMotorControl(void);
    void action_stepperFeed(void);
    void action_cutterMotorControl(void);
    void action_cutterSpeedCutting(void);
    void action_cutterSpeedReturn(void);
    void action_cutterHandwheelPosition(void);
    
    typedef void (EventLoop::*ActionType)(void);
    
    struct ActionMap {
        uint8_t address;
        uint8_t code;
        ActionType callback;
    };

    const uint8_t ADDRESS_ERROR = 0xFF;
    const uint8_t SYSTEM_ADDRESS = 0x81;
    const uint8_t SYSTEM_RESPOND = 0x18;
    const uint8_t SYSTEM_GET_PARTID = 0xF1;
    const uint8_t SYSTEM_GET_VERSION = 0xF5;

    const uint8_t STEPPER_ADDRESS = 0x41;
    const uint8_t STEPPER_RESPOND = 0x14;
    const uint8_t STEPPER_FEEDRATE_MOTOR_CONTROL = 0x20;
    const uint8_t STEPPER_FEED = 0x23;
    const uint32_t STEPPER_FEEDRATE_MAX_POSITION = 200000;
    
    const uint8_t CUTTER_ADDRESS = 0x51;
    const uint8_t CUTTER_RESPOND = 0x15;
    const uint8_t CUTTER_MOTOR = 0x20;
    const uint8_t CUTTER_SPEED_CUTTING = 0x30;
    const uint8_t CUTTER_SPEED_RETURN = 0x31;
    const uint8_t CUTTER_HANDWHEEL_POSITION = 0x40;

    const uint8_t STATUS_NULL = 0x00;
    const uint8_t STATUS_SET = 0x01;
    const uint8_t STATUS_GET = 0xFF;
    const uint8_t STATUS_INVALID_CALIBRATION = 0xE0;
    
    static const uint8_t ACTION_MAP_SIZE = 9;

    const uint32_t DT_REPORT_HANDWHEEL = 1000;

    ActionMap m_actions[ACTION_MAP_SIZE] = {
        {SYSTEM_ADDRESS, SYSTEM_GET_PARTID, &EventLoop::action_systemGetPartId},
        {SYSTEM_ADDRESS, SYSTEM_GET_VERSION, &EventLoop::action_systemGetVersion},
        {STEPPER_ADDRESS, STEPPER_FEEDRATE_MOTOR_CONTROL, &EventLoop::action_stepperFeedrateMotorControl},
        {STEPPER_ADDRESS, STEPPER_FEED, &EventLoop::action_stepperFeed},
        {CUTTER_ADDRESS, CUTTER_MOTOR, &EventLoop::action_cutterMotorControl},
        {CUTTER_ADDRESS, CUTTER_SPEED_CUTTING, &EventLoop::action_cutterSpeedCutting},
        {CUTTER_ADDRESS, CUTTER_SPEED_RETURN, &EventLoop::action_cutterSpeedReturn},
        {CUTTER_ADDRESS, CUTTER_HANDWHEEL_POSITION, &EventLoop::action_cutterHandwheelPosition},
        {0, 0, 0}
    };
};

#endif /* EVENTLOOP_H */
