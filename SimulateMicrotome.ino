#include "serialport.h"
#include "eventslayout.h"

const uint16_t buffer_size = 64;
char buffer_message[buffer_size];
char buffer_read[buffer_size];
char buffer_write[buffer_size];

auto srPort = SerialPort(buffer_message, buffer_size);
auto evLayout = EventsLayout(buffer_write, buffer_read, buffer_size);

void setup()
{
  srPort.open(9600);
}


void loop()
{
  if (srPort.poll()) {
    evLayout.dispatch(srPort.message());
  }

  if (evLayout.poll()){
    uint8_t status = SerialPackage::parse(buffer_message, evLayout.respond());
    Serial.print("STATUS: ");
    Serial.println(status, HEX);
    srPort.send(evLayout.respond());
  }
}
