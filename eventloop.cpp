#include "eventloop.h"

EventLoop::EventLoop(char *buffer, const uint16_t bufferSize) :
    m_flagReady(false),
    m_flagMotor(false),
    m_buffer(buffer),
    p_buffer_read(nullptr),
    p_buffer_write(nullptr),
    m_handwheelState(0),
    m_bufferSize(bufferSize),
    m_feed(0),
    m_speedCut(0),
    m_speedReturn(0),
    m_feedPosition(STEPPER_FEEDRATE_MAX_POSITION),
    m_timestampHandwheel(0)
{
    
}


void EventLoop::dispatch(const char *message)
{
    // re-initialize buffer read/write pointers
    p_buffer_read = m_buffer;
    p_buffer_write = m_buffer;

    // parse message
    uint8_t status = SerialPackage::parse(p_buffer_write, message);
    if (status != SerialPackage::STATUS_DONE) {
        action_error(status);
        return;
    }

    // parse address
    uint8_t address = 0;
    p_buffer_read = SerialPackage::unpack(address, p_buffer_read);

    // parse code
    uint8_t code = 0;
    p_buffer_read = SerialPackage::unpack(code, p_buffer_read);

    // find action
    ActionMap *iter;
    for (iter = m_actions; iter->callback; iter++) {
        if ((iter->address == address) && (iter->code == code)) {
            (this->*iter->callback)();
            break;
        }
    }

    // respond ready
    m_flagReady = true;
}


bool EventLoop::process()
{
    bool flagEvent = false;

    if (m_flagMotor) {

        if ((millis() - m_timestampHandwheel) > DT_REPORT_HANDWHEEL) {
            m_handwheelState++;
            if (m_handwheelState > 0x03)
                m_handwheelState = 0;

            p_buffer_write = m_buffer;
            memset(p_buffer_write, 0, m_bufferSize);
            action_cutterHandwheelPosition();
            m_flagReady = true;
            m_timestampHandwheel = millis();
        }
        
    }
    else {
        m_timestampHandwheel = millis();
    }
    
    if (m_flagReady) {
        m_flagReady = false;
        SerialPackage::package(p_buffer_write, m_buffer);
        flagEvent = true;
    }
    
    return flagEvent;
}


const char *EventLoop::respond()
{
    return m_buffer;
}


void EventLoop::action_error(uint8_t errorCode)
{
    p_buffer_write = m_buffer;
    p_buffer_write = SerialPackage::pack(p_buffer_write, ADDRESS_ERROR);
    p_buffer_write = SerialPackage::pack(p_buffer_write, errorCode);
    m_flagReady = true;
}


void EventLoop::action_systemGetPartId()
{
    uint8_t panelId = 0x06;
    uint32_t partId = 0x55831933;
    p_buffer_write = SerialPackage::pack(p_buffer_write, SYSTEM_RESPOND);
    p_buffer_write = SerialPackage::pack(p_buffer_write, SYSTEM_GET_PARTID);
    p_buffer_write = SerialPackage::pack(p_buffer_write, panelId);
    p_buffer_write = SerialPackage::pack(p_buffer_write, partId);
    
}


void EventLoop::action_systemGetVersion()
{
    uint8_t revisionHardware = 0xAB;
    uint8_t revisionMajor = 0xCD;
    uint8_t revisionMinor = 0xEF;
    p_buffer_write = SerialPackage::pack(p_buffer_write, SYSTEM_RESPOND);
    p_buffer_write = SerialPackage::pack(p_buffer_write, SYSTEM_GET_VERSION);
    p_buffer_write = SerialPackage::pack(p_buffer_write, revisionHardware);
    p_buffer_write = SerialPackage::pack(p_buffer_write, revisionMajor);
    p_buffer_write = SerialPackage::pack(p_buffer_write, revisionMinor);
}


void EventLoop::action_stepperFeedrateMotorControl()
{
    m_feedPosition -= m_feed;
    if (m_feedPosition < (10 * m_feed))
        m_feedPosition = STEPPER_FEEDRATE_MAX_POSITION;
    
    p_buffer_write = SerialPackage::pack(p_buffer_write, STEPPER_RESPOND);
    p_buffer_write = SerialPackage::pack(p_buffer_write, STEPPER_FEEDRATE_MOTOR_CONTROL);
    p_buffer_write = SerialPackage::pack(p_buffer_write, STATUS_GET);
    p_buffer_write = SerialPackage::pack(p_buffer_write, m_feedPosition);
}


void EventLoop::action_stepperFeed()
{
    uint8_t state = STATUS_GET;
    p_buffer_read = SerialPackage::unpack(state, p_buffer_read);
    if (state == STATUS_SET)
        p_buffer_read = SerialPackage::unpack(m_feed, p_buffer_read);

    p_buffer_write = SerialPackage::pack(p_buffer_write, STEPPER_RESPOND);
    p_buffer_write = SerialPackage::pack(p_buffer_write, STEPPER_FEED);
    p_buffer_write = SerialPackage::pack(p_buffer_write, state);
    p_buffer_write = SerialPackage::pack(p_buffer_write, m_feed);
}


void EventLoop::action_cutterMotorControl()
{
    uint8_t state = STATUS_GET;
    
    p_buffer_read = SerialPackage::unpack(state, p_buffer_read);
    if (state == STATUS_NULL) {
        m_flagMotor = false;
    }
    else if (state == STATUS_SET) {
        m_flagMotor = true;
    }

    uint8_t stateResult = (m_flagMotor) ? STATUS_SET : STATUS_NULL;
    p_buffer_write = SerialPackage::pack(p_buffer_write, CUTTER_RESPOND);
    p_buffer_write = SerialPackage::pack(p_buffer_write, CUTTER_MOTOR);
    p_buffer_write = SerialPackage::pack(p_buffer_write, state);
    p_buffer_write = SerialPackage::pack(p_buffer_write, stateResult);
    
}


void EventLoop::action_cutterSpeedCutting()
{
    uint8_t state = STATUS_GET;
    p_buffer_read = SerialPackage::unpack(state, p_buffer_read);
    if (state == STATUS_SET)
        p_buffer_read = SerialPackage::unpack(m_speedCut, p_buffer_read);

    p_buffer_write = SerialPackage::pack(p_buffer_write, CUTTER_RESPOND);
    p_buffer_write = SerialPackage::pack(p_buffer_write, CUTTER_SPEED_CUTTING);
    p_buffer_write = SerialPackage::pack(p_buffer_write, state);
    p_buffer_write = SerialPackage::pack(p_buffer_write, m_speedCut);
}


void EventLoop::action_cutterSpeedReturn()
{
    uint8_t state = STATUS_GET;
    p_buffer_read = SerialPackage::unpack(state, p_buffer_read);
    if (state == STATUS_SET)
        p_buffer_read = SerialPackage::unpack(m_speedReturn, p_buffer_read);

    p_buffer_write = SerialPackage::pack(p_buffer_write, CUTTER_RESPOND);
    p_buffer_write = SerialPackage::pack(p_buffer_write, CUTTER_SPEED_RETURN);
    p_buffer_write = SerialPackage::pack(p_buffer_write, state);
    p_buffer_write = SerialPackage::pack(p_buffer_write, m_speedReturn);
}


void EventLoop::action_cutterHandwheelPosition()
{
    p_buffer_write = SerialPackage::pack(p_buffer_write, CUTTER_RESPOND);
    p_buffer_write = SerialPackage::pack(p_buffer_write, CUTTER_HANDWHEEL_POSITION);
    p_buffer_write = SerialPackage::pack(p_buffer_write, STATUS_GET);
    p_buffer_write = SerialPackage::pack(p_buffer_write, m_handwheelState);
}
