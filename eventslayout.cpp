#include "eventslayout.h"

EventsLayout::EventsLayout(char *buffer_write, char *buffer_read, uint16_t buffer_size) :
    m_flagReady(false),
    m_flagMotor(false),
    m_buffer_write(buffer_write),
    p_buffer_write(buffer_write),
    m_buffer_read(buffer_read),
    p_buffer_read(buffer_read),
    m_handwheelState(0),
    m_buffer_size(buffer_size),
    m_feed(0),
    m_speedCut(0),
    m_speedReturn(0),
    m_feedAdvance(0),
    m_timestampHandwheel(0)
{

}


void EventsLayout::dispatch(const char *message)
{
    // re-initialize buffer read/write
    memset(m_buffer_write, '\0', m_buffer_size);
    memset(m_buffer_read, '\0', m_buffer_size);
    p_buffer_write = m_buffer_write;
    p_buffer_read = m_buffer_read;
    
    // parse message
    uint8_t status = SerialPackage::parse(m_buffer_read, message);
    if (status != SerialPackage::STATUS_DONE) {
        action_error(status);
        return;
    }

    // parse address and code
    uint8_t address = 0;
    uint8_t code = 0;
    p_buffer_read = SerialPackage::unpack(address, p_buffer_read);
    p_buffer_read = SerialPackage::unpack(code, p_buffer_read);

    // call action
    ActionMap *iter;
    for (iter = m_actions; iter->callback; iter++) {
        if ((iter->address == address) && (iter->code == code)) {
            (this->*iter->callback)();
            break;
        }
    }

    // respond is ready
    m_flagReady = true;
}


bool EventsLayout::poll()
{
    bool flagEvent = false;

    if (m_flagMotor) {

        if ((millis() - m_timestampHandwheel) > 1000) {
            m_handwheelState++;
            if (m_handwheelState > 0x03)
                m_handwheelState = 0;

            action_cutterHandwheelPosition();
            m_timestampHandwheel = millis();
        }
        
    }
    

    if (m_flagReady) {
        m_flagReady = false;
        flagEvent = true;
        SerialPackage::package(p_buffer_write, m_buffer_write);
    }

    return flagEvent;
}


const char *EventsLayout::respond()
{
    return m_buffer_write;
}


void EventsLayout::action_error(uint8_t errorCode)
{
    p_buffer_write = m_buffer_write;
    p_buffer_write = SerialPackage::pack(p_buffer_write, SYSTEM_RESPOND);
    p_buffer_write = SerialPackage::pack(p_buffer_write, SYSTEM_CODE_CommandTransmissionError);
    p_buffer_write = SerialPackage::pack(p_buffer_write, errorCode);
    m_flagReady = true;
}


void EventsLayout::action_systemGetPartId(void)
{
    uint8_t panelId = 0x06;
    uint32_t partId = 0x55831933;
    p_buffer_write = SerialPackage::pack(p_buffer_write, SYSTEM_RESPOND);
    p_buffer_write = SerialPackage::pack(p_buffer_write, SYSTEM_CODE_GetPartId);
    p_buffer_write = SerialPackage::pack(p_buffer_write, panelId);
    p_buffer_write = SerialPackage::pack(p_buffer_write, partId);
}


void EventsLayout::action_systemGetVersion(void)
{
    uint8_t revisionHardware = 0xAB;
    uint8_t revisionMajor = 0xCD;
    uint8_t revisionMinor = 0xEF;
    p_buffer_write = SerialPackage::pack(p_buffer_write, SYSTEM_RESPOND);
    p_buffer_write = SerialPackage::pack(p_buffer_write, SYSTEM_CODE_GetVersion);
    p_buffer_write = SerialPackage::pack(p_buffer_write, revisionHardware);
    p_buffer_write = SerialPackage::pack(p_buffer_write, revisionMajor);
    p_buffer_write = SerialPackage::pack(p_buffer_write, revisionMinor);
}


void EventsLayout::action_stepperFeedrateMotorControl(void)
{
    m_feedAdvance += m_feed;
    if (m_feedAdvance >= STEPPER_FeedrateMaxPosition)
        m_feedAdvance = 0;
    
    p_buffer_write = SerialPackage::pack(p_buffer_write, STEPPER_RESPOND);
    p_buffer_write = SerialPackage::pack(p_buffer_write, STEPPER_CODE_FeedrateMotorControl);
    p_buffer_write = SerialPackage::pack(p_buffer_write, STATUS_GET);
    p_buffer_write = SerialPackage::pack(p_buffer_write, m_feedAdvance);
}


void EventsLayout::action_stepperFeed(void)
{
    uint8_t state = STATUS_GET;
    p_buffer_read = SerialPackage::unpack(state, p_buffer_read);
    if (state == STATUS_SET)
        p_buffer_read = SerialPackage::unpack(m_feed, p_buffer_read);

    p_buffer_write = SerialPackage::pack(p_buffer_write, STEPPER_RESPOND);
    p_buffer_write = SerialPackage::pack(p_buffer_write, STEPPER_CODE_Feed);
    p_buffer_write = SerialPackage::pack(p_buffer_write, state);
    p_buffer_write = SerialPackage::pack(p_buffer_write, m_feed);
}


void EventsLayout::action_cutterMotorControl(void)
{
    uint8_t state = STATUS_GET;
    
    p_buffer_read = SerialPackage::unpack(state, p_buffer_read);
    if (state == STATUS_NULL) {
        m_flagMotor = false;
    }
    else if (state == STATUS_SET) {
        m_flagMotor = true;
        m_timestampHandwheel = millis();
    }

    uint8_t stateResult = (m_flagMotor) ? STATUS_SET : STATUS_NULL;
    p_buffer_write = SerialPackage::pack(p_buffer_write, CUTTER_RESPOND);
    p_buffer_write = SerialPackage::pack(p_buffer_write, CUTTER_CODE_CuttingMotorControl);
    p_buffer_write = SerialPackage::pack(p_buffer_write, state);
    p_buffer_write = SerialPackage::pack(p_buffer_write, stateResult);
}


void EventsLayout::action_cutterSpeedCutting(void)
{
    uint8_t state = STATUS_GET;
    p_buffer_read = SerialPackage::unpack(state, p_buffer_read);
    if (state == STATUS_SET)
        p_buffer_read = SerialPackage::unpack(m_speedCut, p_buffer_read);

    p_buffer_write = SerialPackage::pack(p_buffer_write, CUTTER_RESPOND);
    p_buffer_write = SerialPackage::pack(p_buffer_write, CUTTER_CODE_CuttingSpeed);
    p_buffer_write = SerialPackage::pack(p_buffer_write, state);
    p_buffer_write = SerialPackage::pack(p_buffer_write, m_speedCut);
}


void EventsLayout::action_cutterSpeedReturn(void)
{
    uint8_t state = STATUS_GET;
    p_buffer_read = SerialPackage::unpack(state, p_buffer_read);
    if (state == STATUS_SET)
        p_buffer_read = SerialPackage::unpack(m_speedReturn, p_buffer_read);

    p_buffer_write = SerialPackage::pack(p_buffer_write, CUTTER_RESPOND);
    p_buffer_write = SerialPackage::pack(p_buffer_write, CUTTER_CODE_ReturnSpeed);
    p_buffer_write = SerialPackage::pack(p_buffer_write, state);
    p_buffer_write = SerialPackage::pack(p_buffer_write, m_speedReturn);
}


void EventsLayout::action_cutterHandwheelPosition(void)
{
    p_buffer_write = m_buffer_write;
    p_buffer_write = SerialPackage::pack(p_buffer_write, CUTTER_RESPOND);
    p_buffer_write = SerialPackage::pack(p_buffer_write, CUTTER_CODE_HandwheelPosition);
    p_buffer_write = SerialPackage::pack(p_buffer_write, STATUS_GET);
    p_buffer_write = SerialPackage::pack(p_buffer_write, m_handwheelState);
    m_flagReady = true;
}
